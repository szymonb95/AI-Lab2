#pragma once
#include "stdafx.h"
#include "MosaicCell.h"

namespace mosaic {

	class MosaicBoard
	{
	private:
		std::vector< std::vector<MosaicCell> > board;
		std::string printBorder(int currentRow);
		std::string printTopBorder();
	public:
		MosaicBoard();
		MosaicBoard(int size);
		~MosaicBoard();
		std::shared_ptr<MosaicBoard> copy();
		MosaicCell* getCell(int row, int column);
		std::vector<MosaicCell*> getRow(int row);
		std::vector<MosaicCell*> getColumn(int column);
		MosaicCell* getFirstEmptyCell();
		void setLetter(int row, int column, Letter letter);
		bool isCompleted();
		bool isLineCompleted(std::vector<MosaicCell*> line);
		bool isValid();
		bool checkLine(std::vector<MosaicCell*> line);
		bool hasDuplicatedLines();
		int lineValue(std::vector<MosaicCell*> line);
		int getSize();
		std::vector<MosaicCell>& operator[](int x);
		friend std::ostream& operator<<(std::ostream& os, MosaicBoard& table);
	};

}