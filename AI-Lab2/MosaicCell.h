#pragma once

namespace mosaic {
	class MosaicBoard;

	enum Letter {
		EMPTY = '-',
		X = 'X',
		Y = 'Y'
	};

	class MosaicCell
	{
		Letter letter;
		int row, column;
		MosaicBoard* board;
	public:
		MosaicCell();
		~MosaicCell();
		Letter getLetter() const;
		MosaicBoard* getBoard();
		int getRow();
		int getColumn();
		void setLetter(Letter letter);
		void setBoard(MosaicBoard* board);
		void setRowAndColumn(int row, int column);
		MosaicCell& operator=(const Letter& letter);
		bool operator==(const MosaicCell& cell);
		bool operator!=(const MosaicCell& cell);
		bool operator==(const Letter& letter);
		bool operator!=(const Letter& letter);
	};

}