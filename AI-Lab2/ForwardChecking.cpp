#include "stdafx.h"
#include "HitoriForwardChecking.h"

namespace hitori {

	void ForwardChecking::searchAdjacentTriplesInLine(std::vector<HitoriCell*> line)
	{
		for (int i = 2; i < line.size(); i++)
		{
			if (*line[i - 2] == *line[i])
			{
				if (*line[i - 2] == *line[i - 1] && *line[i] == *line[i - 1])
				{
					if (!line[i]->hasState())
					{
						line[i]->setIsIncorrect(true);
						line[i]->getTable()->unshadeAround(line[i]);
					}
					if (!line[i]->hasState())
					{
						line[i - 2]->setIsIncorrect(true);
						line[i - 2]->getTable()->unshadeAround(line[i - 2]);
					}
				}
				else
				{
					if (!line[i - 1]->hasState())
						line[i - 1]->setIsCorrect(true);
				}
			}
		}
	}

	ForwardChecking::ForwardChecking()
	{
	}

	void ForwardChecking::searchAdjacentTriples(HitoriBoard * table)
	{
		if (table->size() >= 3)
		{
			for (int i = 0; i < table->size(); i++)
			{
				searchAdjacentTriplesInLine(table->getRow(i));
				searchAdjacentTriplesInLine(table->getColumn(i));
			}
		}
	}

	void ForwardChecking::pairInduction(HitoriBoard * table)
	{
		if (table->size() >= 4)
		{
			for (int i = 0; i < table->size(); i++)
			{
				pairInductionInLine(table->getRow(i));
				pairInductionInLine(table->getColumn(i));
			}
		}
	}

	void ForwardChecking::checkLines(HitoriBoard * table)
	{
		bool boardChanged = true;
		while (boardChanged)
		{
			boardChanged = false;
			for (int i = 0; i < table->size(); i++)
			{
				if (boardChanged)
				{
					checkLine(table->getRow(i));
				}
				else
				{
					boardChanged = checkLine(table->getRow(i));
				}
				if (boardChanged)
				{
					checkLine(table->getColumn(i));
				}
				else
				{
					boardChanged = checkLine(table->getColumn(i));
				}
			}
		};
	}

	void ForwardChecking::satisfyConstraints(HitoriBoard * table)
	{
		ForwardChecking::searchAdjacentTriples(table);
		ForwardChecking::pairInduction(table);
		ForwardChecking::checkLines(table);
	}

	void ForwardChecking::pairInductionInLine(std::vector<HitoriCell*> line)
	{
		std::map<int, std::pair<bool, std::vector<int> > > repeats;
		for (int i = 0; i < line.size(); i++)
		{
			int value = line[i]->getValue();
			if (repeats[value].second.size() != 0) {
				if (line[i - 1]->getValue() == value)
				{
					repeats[value].second.pop_back();
					repeats[value].first = true;
				}
				else
				{
					repeats[value].second.push_back(i);
				}
			}
			else
			{
				repeats[value].second.push_back(i);
			}
		}
		for (auto repeat : repeats)
		{
			if (repeat.second.first)
			{
				for (int i = 0; i < repeat.second.second.size(); i++)
				{
					HitoriCell * cell = line[repeat.second.second[i]];
					if (!cell->hasState())
					{
						cell->shade();
						cell->getTable()->unshadeAround(cell);
					}
				}
			}
		}
	}

	// return true if board changed
	bool ForwardChecking::checkLine(std::vector<HitoriCell*> line)
	{
		bool boardChanged = false;
		using Cells = std::vector<HitoriCell*>;
		std::map<int, std::pair<bool, Cells > > repeats;
		for (HitoriCell* cell : line)
		{
			int value = cell->getValue();
			if (repeats[value].second.size() == 0)
			{
				repeats[value].first = false;
			}
			if (!cell->isIncorrect())
			{
				if (cell->isCorrect())
				{
					repeats[value].first = true;
					for (HitoriCell* repeatedCell : repeats[value].second)
					{
						repeatedCell->shade();
						repeatedCell->getTable()->unshadeAround(repeatedCell);
						boardChanged = true;
					}
				}
				else if (repeats[value].first)
				{
					cell->shade();
					cell->getTable()->unshadeAround(cell);
					boardChanged = true;
				}
				repeats[value].second.push_back(cell);
			}
		}
		return boardChanged;
	}

	bool ForwardChecking::hasSameNeighbour(std::vector<HitoriCell*> line, int cell)
	{
		if (cell == 0)
		{
			return line[0] == line[1];
		}
		else if (cell == line.size() - 1)
		{
			return line[cell - 1] == line[cell];
		}
		else
		{
			return line[cell - 1] == line[cell] || line[cell] == line[cell + 1];
		}
	}


	ForwardChecking::~ForwardChecking()
	{
	}
}