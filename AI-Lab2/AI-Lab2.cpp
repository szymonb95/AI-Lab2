// AI-Lab2.cpp : Defines the entry point for the console application.
//

#include <cstdlib>
#include "stdafx.h"
#include "HitoriBoard.h"
#include "HitoriForwardChecking.h"
#include "HitoriBacktracking.h"
#include "MosaicBoard.h"
#include "MosaicBacktracking.h"
#include "Games.h"


enum Algorithm {
	MOSAIC, HITORI
};

void runHitori(int boardNo);
void runMosaic(int size);
void runAlgorithm(Algorithm a, int no);

int main(int argc, char *argv[])
{
	int size = 6;
	Algorithm a = MOSAIC;
	if (argc > 1)
	{
		if (argv[1][0] == 'h')
		{
			if (argc == 3)
			{
				runHitori(std::stoi(argv[2]));
			}
		} 
		else
		{
			if (argc == 3)
			{
				runMosaic(std::stoi(argv[2]));
			}
			else 
			{
				runMosaic(size);
			}
		}
	}
	else
	{
		runAlgorithm(a, size);
	}
	//system("pause");
	return 0;
}

void runHitori(int boardNo)
{
	auto tab = hitori::getBoard(boardNo);
	std::cout << *tab << std::endl;
	//system("pause");
	if (tab.get()->getFirstCellWithoutState())
	{
		clock_t begin = clock();
		tab = hitori::Backtracking::run(tab);
		clock_t end = clock();
		double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
		if (tab.get())
			std::cout << "Solution found:" << std::endl << *tab << std::endl;
		else
			std::cout << "Solution not found." << std::endl << std::endl;
		std::cout << "Elapsed time: " << elapsed_secs << "s" << std::endl;
	}
}

void runMosaic(int size)
{
	auto board = std::make_shared<mosaic::MosaicBoard>(size);
	std::cout << *board << std::endl;
	clock_t begin = clock();
	board = mosaic::MosaicBacktracking::run(board);
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	if (board.get())
		std::cout << "Solution found:" << std::endl << *board << std::endl;
	else
		std::cout << "Solution not found." << std::endl << std::endl;
	std::cout << "Elapsed time: " << elapsed_secs << "s" << std::endl;
}

void runAlgorithm(Algorithm a, int no)
{
	switch (a)
	{
	case MOSAIC:
		runMosaic(no);
		break;
	case HITORI:
		runHitori(no);
	}
}
