#include "stdafx.h"
#include "MosaicCell.h"

namespace mosaic {

	MosaicCell::MosaicCell()
	{
	}


	MosaicCell::~MosaicCell()
	{
	}

	Letter MosaicCell::getLetter() const
	{
		return letter;
	}

	MosaicBoard* MosaicCell::getBoard()
	{
		return board;
	}

	int MosaicCell::getRow()
	{
		return row;
	}

	int MosaicCell::getColumn()
	{
		return column;
	}

	void MosaicCell::setLetter(Letter letter)
	{
		this->letter = letter;
	}

	void MosaicCell::setBoard(MosaicBoard* board)
	{
		this->board = board;
	}

	void MosaicCell::setRowAndColumn(int row, int column)
	{
		this->row = row;
		this->column = column;
	}

	MosaicCell & MosaicCell::operator=(const Letter & letter)
	{
		this->letter = letter;
		return *this;
	}

	bool MosaicCell::operator==(const MosaicCell & cell)
	{
		return this->getLetter() == cell.getLetter();
	}

	bool MosaicCell::operator!=(const MosaicCell & cell)
	{
		return this->getLetter() != cell.getLetter();
	}

	bool MosaicCell::operator==(const Letter & letter)
	{
		return this->getLetter() == letter;
	}

	bool MosaicCell::operator!=(const Letter & letter)
	{
		return this->getLetter() != letter;
	}

}