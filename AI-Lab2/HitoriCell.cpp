#include "stdafx.h"
#include "HitoriCell.h"

namespace hitori {

	HitoriCell::HitoriCell()
	{
	}

	HitoriCell::HitoriCell(int value) : row(-1), column(-1), table(nullptr), value(value), incorrect(false), correct(false)
	{
	}

	HitoriCell::HitoriCell(int row, int column, HitoriBoard * table, int value) : row(row), column(column), table(table), value(value), incorrect(false), correct(false)
	{
	}

	std::string HitoriCell::toString() const
	{
		return std::to_string(value);
	}

	int HitoriCell::getValue() const
	{
		return value;
	}

	int HitoriCell::getColumn() const
	{
		return column;
	}

	int HitoriCell::getRow() const
	{
		return row;
	}

	bool HitoriCell::hasState()
	{
		return correct || incorrect;
	}

	bool HitoriCell::isIncorrect() const
	{
		return incorrect;
	}

	bool HitoriCell::isCorrect() const
	{
		return correct;
	}

	HitoriBoard * HitoriCell::getTable()
	{
		return table;
	}

	void HitoriCell::setRowColumn(int row, int column)
	{
		this->row = row;
		this->column = column;
	}

	void HitoriCell::setIsIncorrect(bool b)
	{
		incorrect = b;
	}

	void HitoriCell::setTable(HitoriBoard* table)
	{
		this->table = table;
	}

	void HitoriCell::shade()
	{
		setIsCorrect(false);
		setIsIncorrect(true);
	}

	void HitoriCell::unshade()
	{
		setIsIncorrect(false);
		setIsCorrect(true);
	}

	void HitoriCell::setIsCorrect(bool b)
	{
		correct = b;
	}

	std::ostream & operator<<(std::ostream & os, const HitoriCell & cell)
	{
		if (cell.incorrect)
		{
			os << white;
		}
		else if (cell.correct)
		{
			os << whiteBackground;
		}
		else
		{
			os << yellowBackground;
		}
		os << cell.toString() << whiteBackground;
		return os;
	}

	HitoriCell & HitoriCell::operator=(const int & value)
	{
		this->value = value;
		return *this;
	}

	bool HitoriCell::operator==(const HitoriCell & cell)
	{
		return this->getValue() == cell.getValue();
	}

	bool HitoriCell::operator!=(const HitoriCell & cell)
	{
		return this->getValue() != cell.getValue();
	}

	HitoriCell::~HitoriCell()
	{
	}
}