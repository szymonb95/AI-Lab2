#pragma once
#include "HitoriBoard.h"
#include "HitoriForwardChecking.h"

namespace hitori {

	using TravelTable = std::vector< std::vector<bool> >;

	class Backtracking
	{
	private:
		static bool checkLines(HitoriBoard * table);
		static bool checkLine(std::vector<HitoriCell*> line);
		static bool hasShadedNeighbour(HitoriCell* cell);
		static bool isContinuous(HitoriBoard *table);
		static void travel(HitoriCell* cell, TravelTable& travelTable);
		static std::shared_ptr<HitoriBoard> spreadChildren(std::shared_ptr<HitoriBoard> table, int level, int& generatedTables, int& returnsFromRecursion);
		static void printState(HitoriBoard* table, HitoriCell* cell, int level, bool isValid, int generatedTables, int returnsInTree);
	public:
		Backtracking();
		~Backtracking();
		static bool isValid(HitoriBoard* table);
		static std::shared_ptr<HitoriBoard> run(std::shared_ptr<HitoriBoard> table);
	};
}

