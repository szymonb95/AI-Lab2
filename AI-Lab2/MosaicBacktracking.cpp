#include "stdafx.h"
#include "MosaicBacktracking.h"

namespace mosaic {
	std::shared_ptr<MosaicBoard> MosaicBacktracking::spreadChildren(std::shared_ptr<MosaicBoard> board, int level, int& generatedTables)
	{
		if (board.get()->isCompleted())
		{
			return board;
		}
		std::shared_ptr<MosaicBoard>currentBoard = board->copy();
		MosaicCell* cell = currentBoard->getFirstEmptyCell();
		cell->setLetter(Letter::X); generatedTables++;
		bool isValid = currentBoard->isValid();
		//printState(currentBoard.get(), cell, level, isValid, generatedTables);
		std::shared_ptr<MosaicBoard>child;
		if (isValid)
		{
			child = spreadChildren(currentBoard, level + 1, generatedTables);
			if (child.get())
				return child;
		}
		cell->setLetter(Letter::Y); generatedTables++;
		isValid = currentBoard->isValid();
		//printState(currentBoard.get(), cell, level, isValid, generatedTables);
		if (isValid)
		{
			child = spreadChildren(currentBoard, level + 1, generatedTables);
			if (child.get())
				return child;
		}
		return nullptr;
	}

	void MosaicBacktracking::printState(MosaicBoard * board, MosaicCell * cell, int level, bool isValid, int tablesGenerated)
	{
		//Sleep(500);
		system("cls");
		printf("Tables generated: %d, Level: %d, Cell: %c (row: %d, column: %d)\n",
			tablesGenerated, level, cell->getLetter(), cell->getRow(), cell->getColumn());
		std::cout << *board;
		std::cout << (isValid ? "valid" : "invalid") << std::endl << std::endl;
	}

	MosaicBacktracking::MosaicBacktracking()
	{
	}


	MosaicBacktracking::~MosaicBacktracking()
	{
	}

	std::shared_ptr<MosaicBoard> MosaicBacktracking::run(std::shared_ptr<MosaicBoard> board)
	{
		int generated = 1;
		std::shared_ptr<MosaicBoard> tab = spreadChildren(board, 1, generated);
		printf("Generated boards: %d\n", generated);
		return tab;
	}

}