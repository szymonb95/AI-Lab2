#pragma once
#include "HitoriCell.h"

namespace hitori {

	using TravelTable = std::vector< std::vector<bool> >;

	class HitoriBoard
	{
	private:
		std::vector< std::vector<HitoriCell> > table;
		std::string printBorder(int currentRow);
		std::string printTopBorder();
	public:
		HitoriBoard();
		HitoriBoard(std::initializer_list <std::initializer_list<HitoriCell> > table);
		std::shared_ptr<HitoriBoard> copy();
		std::vector< std::vector<HitoriCell> >* getTable();
		HitoriCell* getCell(int row, int column);
		HitoriCell* getFirstNotIncorrectCell();
		HitoriCell* getNextNotIncorrectCell(HitoriCell* cell);
		HitoriCell* getNextNotIncorrectAndUntravelledCell(HitoriCell* cell, TravelTable& travelTable);
		HitoriCell* getFirstCellWithoutState();
		std::vector<HitoriCell*> getRow(int row);
		std::vector<HitoriCell*> getColumn(int column);
		int size() const;
		std::string toString();
		int notIncorrectCells();
		void unshadeAround(HitoriCell* cell);
		std::vector<HitoriCell>& operator[](int x);
		friend std::ostream& operator<<(std::ostream& os, HitoriBoard& table);
		~HitoriBoard();
	};

}