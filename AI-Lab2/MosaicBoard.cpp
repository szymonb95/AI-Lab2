#include "stdafx.h"
#include "MosaicBoard.h"

namespace mosaic {

	std::string MosaicBoard::printBorder(int currentRow)
	{
		std::string s;
		for (int j = 0; j < board.size(); j++)
		{
			if (currentRow == board.size() - 1)
			{
				if (j == 0)
				{
					s += (char)192;// ?
				}
				else
				{
					s += (char)193;// ?
				}
			}
			else
			{
				if (j == 0)
				{
					s += (char)195;// ?
				}
				else
				{
					s += (char)197;// ?
				}
			}
			s += (char)196;// ?
		}
		if (currentRow == board.size() - 1)
		{
			s += (char)217;// ?
		}
		else
		{
			s += (char)180;// ?
		}
		return s;
	}

	std::string MosaicBoard::printTopBorder()
	{
		std::string s;
		for (int j = 0; j < board.size(); j++)
		{
			if (j == 0)
			{
				s += (char)218;// ?
			}
			else
			{
				s += (char)194;// ?
			}
			s += (char)196;// ?
		}
		s += (char)191;// ?
		return s;
	}

	std::ostream & operator<<(std::ostream & os, MosaicBoard & board)
	{
		os << board.printTopBorder() << std::endl;
		for (int i = 0; i < board.getSize(); i++)
		{
			os << (char)179;// ?
			for (MosaicCell letter : board[i])
			{
				os << (char)letter.getLetter() << (char)179;// ?
			}
			os << std::endl;
			os << board.printBorder(i) << std::endl;
		}
		return os;
	}

	MosaicBoard::MosaicBoard()
	{
	}

	MosaicBoard::MosaicBoard(int size) : 
		board(std::vector< std::vector<MosaicCell> >(size, std::vector<MosaicCell>(size, MosaicCell())))
	{
		for (int row = 0; row < getSize(); row++)
		{
			for (int column = 0; column < getSize(); column++)
			{
				MosaicCell* cell = getCell(row, column);
				*cell = Letter::EMPTY;
				cell->setRowAndColumn(row, column);
				cell->setBoard(this);
			}
		}
	}


	MosaicBoard::~MosaicBoard()
	{
	}

	std::shared_ptr<MosaicBoard> MosaicBoard::copy()
	{
		std::shared_ptr<MosaicBoard> newBoard(new MosaicBoard(*this));
		for (int row = 0; row < newBoard.get()->getSize(); row++)
		{
			for (int column = 0; column < newBoard.get()->getSize(); column++)
			{
				newBoard->getCell(row, column)->setBoard(newBoard.get());
			}
		}
		return newBoard;
	}

	MosaicCell* MosaicBoard::getCell(int row, int column)
	{
		return &board[row][column];
	}

	std::vector<MosaicCell*> MosaicBoard::getRow(int row)
	{
		std::vector<MosaicCell*> v;
		for (int i = 0; i < getSize(); i++)
		{
			v.push_back(&board[row][i]);
		}
		return v;
	}

	std::vector<MosaicCell*> MosaicBoard::getColumn(int column)
	{
		std::vector<MosaicCell*> v;
		for (int i = 0; i < getSize(); i++)
		{
			v.push_back(&board[i][column]);
		}
		return v;
	}

	MosaicCell * MosaicBoard::getFirstEmptyCell()
	{
		for (int row = 0; row < getSize(); row++)
		{
			for (int column = 0; column < getSize(); column++)
			{
				MosaicCell *cell = getCell(row, column);
				if (*cell == Letter::EMPTY)
					return cell;
			}
		}
		return nullptr;
	}

	void MosaicBoard::setLetter(int row, int column, Letter letter)
	{
		board[row][column] = letter;
	}

	bool MosaicBoard::isCompleted()
	{
		return getFirstEmptyCell() == nullptr;
	}

	bool MosaicBoard::isLineCompleted(std::vector<MosaicCell*> line)
	{
		for (MosaicCell* cell : line)
		{
			if (*cell == Letter::EMPTY)
				return false;
		}
		return true;
	}

	bool MosaicBoard::isValid()
	{
		int size = getSize();
		for (int i = 0; i < size; i++)
		{
			if (!checkLine(getRow(i)) || !checkLine(getColumn(i)))
				return false;
		}
		if (hasDuplicatedLines())
			return false;
		return true;
	}

	bool MosaicBoard::checkLine(std::vector<MosaicCell*> line)
	{
		std::vector<int> repeats[2];
		for (int i = 0; i < line.size(); i++)
		{
			if (*line[i]  == Letter::X)
			{
				repeats[0].push_back(i);
				if (repeats[0].size() > 2)
				{
					int x1 = repeats[0].end()[-3] + 2;
					int x2 = repeats[0].end()[-2] + 1;
					int x3 = repeats[0].end()[-1];
					if (x1 == x2 && x2 == x3 && x1 == x3)
					{
						return false;
					}
				}
			}
			else if (*line[i] == Letter::Y)
			{
				repeats[1].push_back(i);
				if (repeats[1].size() > 2)
				{
					int y1 = repeats[1].end()[-3] + 2;
					int y2 = repeats[1].end()[-2] + 1;
					int y3 = repeats[1].end()[-1];
					if (y1 == y2 && y2 == y3 && y1 == y3)
					{
						return false;
					}
				}
			}
		}
		return true;
	}

	bool MosaicBoard::hasDuplicatedLines()
	{
		std::set<int> rowValues, columnValues;
		for (int i = 0; i < getSize(); i++)
		{
			std::vector<MosaicCell*> currentLine = getRow(i);
			if (isLineCompleted(currentLine))
			{
				if (rowValues.insert(lineValue(currentLine)).second) {
					currentLine = getColumn(i);
					if (isLineCompleted(currentLine))
						if (!columnValues.insert(lineValue(currentLine)).second)
						{
							return true;
						}
				}
				else
				{

					return true;
				}
			}
		}
		return false;
	}

	int MosaicBoard::lineValue(std::vector<MosaicCell*> line)
	{
		int value = 0;
		int size = line.size();
		for (int i = 0; i < size; i++)
		{
			int v = *line[i] == Letter::X ? 0 : 2;
			value += (int)std::pow(v, i);
		}
		return value;
	}

	int MosaicBoard::getSize()
	{
		return board.size();
	}

	std::vector<MosaicCell>& MosaicBoard::operator[](int x)
	{
		return board[x];
	}

}