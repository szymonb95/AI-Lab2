#pragma once
#include "MosaicBoard.h"

namespace mosaic {
	class MosaicBacktracking
	{
	private:
		static std::shared_ptr<MosaicBoard> spreadChildren(std::shared_ptr<MosaicBoard> board, int level, int& generatedTables);
		static void printState(MosaicBoard* board, MosaicCell* cell, int level, bool isValid, int tablesGenerated);
	public:
		MosaicBacktracking();
		~MosaicBacktracking();
		static std::shared_ptr<MosaicBoard> run(std::shared_ptr<MosaicBoard> board);
	};
}

