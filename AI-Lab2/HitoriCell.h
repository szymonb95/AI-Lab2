#pragma once

namespace hitori {

	class HitoriBoard;

	class HitoriCell
	{
	private:
		int value;
		int column;
		int row;
		HitoriBoard* table;
		bool correct;
		bool incorrect;
	public:
		HitoriCell();
		HitoriCell(int value);
		HitoriCell(int row, int column, HitoriBoard* table, int value = 0);
		std::string toString() const;
		int getValue() const;
		int getColumn() const;
		int getRow() const;
		bool hasState();
		bool isIncorrect() const;
		bool isCorrect() const;
		HitoriBoard* getTable();
		void setRowColumn(int row, int column);
		void setIsIncorrect(bool b);
		void setTable(HitoriBoard* table);
		void shade();
		void unshade();
		void setIsCorrect(bool b);
		friend std::ostream& operator<<(std::ostream& os, const HitoriCell& cell);
		HitoriCell& operator=(const int& value);
		bool operator==(const HitoriCell& cell);
		bool operator!=(const HitoriCell& cell);
		~HitoriCell();
	};
}