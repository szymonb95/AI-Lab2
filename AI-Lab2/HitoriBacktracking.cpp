#include "stdafx.h"
#include "HitoriBacktracking.h"

namespace hitori {

	bool Backtracking::isValid(HitoriBoard* table)
	{
		if (checkLines(table))
		{
			return isContinuous(table);
		}
		return false;
	}

	std::shared_ptr<HitoriBoard> Backtracking::run(std::shared_ptr<HitoriBoard> table)
	{
		int generatedTables = 1;
		int returnsInTree = 0;
		std::shared_ptr<HitoriBoard> tab = spreadChildren(table, 1, generatedTables, returnsInTree);
		printf("Generated boards: %d, Returns in tree: %d\n", generatedTables, returnsInTree);
		return tab;
	}

	bool Backtracking::checkLine(std::vector<HitoriCell*> line)
	{
		using Cells = std::vector<HitoriCell*>;
		std::map<int, HitoriCell*> repeats;
		for (HitoriCell* cell : line)
		{
			if (cell->isCorrect())
			{
				if (repeats.find(cell->getValue()) == repeats.end())
				{
					repeats[cell->getValue()] = cell;
				}
				else
				{
					return false;
				}
			}
			else
			{
				if (cell->isIncorrect() && hasShadedNeighbour(cell))
				{
					return false;
				}
			}
		}
		return true;
	}

	bool Backtracking::hasShadedNeighbour(HitoriCell * cell)
	{
		int row = cell->getRow();
		int column = cell->getColumn();
		HitoriBoard* table = cell->getTable();
		HitoriCell* neighbour = table->getCell(row - 1, column);
		if (neighbour)
		{
			if (neighbour->isIncorrect())
				return true;
		}
		neighbour = table->getCell(row + 1, column);
		if (neighbour)
		{
			if (neighbour->isIncorrect())
				return true;
		}
		neighbour = table->getCell(row, column - 1);
		if (neighbour)
		{
			if (neighbour->isIncorrect())
				return true;
		}
		neighbour = table->getCell(row, column + 1);
		if (neighbour)
		{
			if (neighbour->isIncorrect())
				return true;
		}
		return false;
	}

	bool Backtracking::isContinuous(HitoriBoard * table)
	{
		int size = table->size();
		TravelTable travelTable(size, std::vector<bool>(size, false));
		int notIncorrectCells = table->notIncorrectCells();
		travel(table->getFirstNotIncorrectCell(), travelTable);
		int travelledCells = 0;
		for (int row = 0; row < travelTable.size(); row++)
		{
			for (int column = 0; column < travelTable.size(); column++)
			{
				if (travelTable[row][column])
					travelledCells++;
			}
		}
		return notIncorrectCells == travelledCells;
	}

	void Backtracking::travel(HitoriCell * cell, TravelTable & travelTable)
	{
		travelTable[cell->getRow()][cell->getColumn()] = true;
		bool travelFinished = false;
		while (!travelFinished)
		{
			HitoriCell* nextCell = cell->getTable()->getNextNotIncorrectAndUntravelledCell(cell, travelTable);
			travelFinished = nextCell ? false : true;
			if (!travelFinished)
				travel(nextCell, travelTable);
		}
	}

	std::shared_ptr<HitoriBoard> Backtracking::spreadChildren(std::shared_ptr<HitoriBoard> table, int level, int& generatedTables, int& returnsInTree)
	{
		if (!table.get()->getFirstCellWithoutState())
		{
			return table;
		}
		std::shared_ptr<HitoriBoard>currentTable = table->copy();
		HitoriCell* cell = currentTable->getFirstCellWithoutState();
		ForwardChecking::satisfyConstraints(currentTable.get());
		for (int i = 0; i < 2; i++)
		{
			i == 0 ? cell->shade() : cell->unshade();
			bool isValid = Backtracking::isValid(currentTable.get());
			//printState(currentTable.get(), cell, level, isValid, generatedTables, returnsInTree);
			std::shared_ptr<HitoriBoard>child;
			if (isValid)
			{
				child = spreadChildren(currentTable, level + 1, generatedTables, returnsInTree);
				if (child.get())
					return child;
			}
			returnsInTree++;
		}
		return nullptr;
	}

	void Backtracking::printState(HitoriBoard* table, HitoriCell* cell, int level, bool isValid, int generatedTables, int returnsInTree)
	{
		//Sleep(500);
		system("cls");
		printf("Tables generated: %d, Level: %d, Cell: %d (row: %d, column: %d) Unshaded: %d\n",
			generatedTables, level, cell->getValue(), cell->getRow(), cell->getColumn(), cell->isCorrect());
		printf("Returns in tree: %d\n", returnsInTree);
		std::cout << *table << white;
		std::cout << (isValid ? "valid" : "invalid") << std::endl << std::endl;
	}

	Backtracking::Backtracking()
	{
	}


	Backtracking::~Backtracking()
	{
	}

	bool Backtracking::checkLines(HitoriBoard * table)
	{
		for (int i = 0; i < table->size(); i++)
		{
			if (!checkLine(table->getRow(i)) || !checkLine(table->getColumn(i)))
			{
				return false;
			}
		}
		return true;
	}
}