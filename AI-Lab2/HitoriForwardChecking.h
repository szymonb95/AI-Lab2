#pragma once
#include "HitoriBoard.h"

namespace hitori {

	class ForwardChecking
	{
	private:
		static void searchAdjacentTriplesInLine(std::vector<HitoriCell*> line);
		static void pairInductionInLine(std::vector<HitoriCell*> line);
		static bool checkLine(std::vector<HitoriCell*> line);
		static bool hasSameNeighbour(std::vector<HitoriCell*> line, int cell);
	public:
		ForwardChecking();
		static void searchAdjacentTriples(HitoriBoard* table);
		static void pairInduction(HitoriBoard* table);
		static void checkLines(HitoriBoard* line);
		static void satisfyConstraints(HitoriBoard* line);
		~ForwardChecking();
	};

}