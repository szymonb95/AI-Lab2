#include "stdafx.h"
#include "HitoriBoard.h"

namespace hitori {

	int numDigits(int number)
	{
		int digits = 0;
		if (number < 0) digits = 1; // remove this line if '-' counts as a digit
		while (number) {
			number /= 10;
			digits++;
		}
		return digits;
	}

	std::string HitoriBoard::printBorder(int currentRow)
	{
		std::string s;
		for (int j = 0; j < table.size(); j++)
		{
			if (currentRow == table.size() - 1)
			{
				if (j == 0)
				{
					s += (char)192;// ?
				}
				else
				{
					s += (char)193;// ?
				}
			}
			else
			{
				if (j == 0)
				{
					s += (char)195;// ?
				}
				else
				{
					s += (char)197;// ?
				}
			}
			for (int i = 0; i < numDigits(table.size()); i++)
				s += (char)196;// ?
		}
		if (currentRow == table.size() - 1)
		{
			s += (char)217;// ?
		}
		else
		{
			s += (char)180;// ?
		}
		return s;
	}

	std::string HitoriBoard::printTopBorder()
	{
		std::string s;
		for (int j = 0; j < table.size(); j++)
		{
			if (j == 0)
			{
				s += (char)218;// ?
			}
			else
			{
				s += (char)194;// ?
			}
			for (int i = 0; i < numDigits(table.size()); i++)
				s += (char)196;// ?
		}
		s += (char)191;// ?
		return s;
	}

	HitoriBoard::HitoriBoard()
		: table(std::vector< std::vector<HitoriCell> >(5, std::vector<HitoriCell>(5)))
	{
	}

	std::shared_ptr<HitoriBoard> HitoriBoard::copy()
	{
		std::shared_ptr<HitoriBoard> newTable(new HitoriBoard(*this));
		for (int row = 0; row < newTable.get()->size(); row++)
		{
			for (int column = 0; column < newTable.get()->size(); column++)
			{
				newTable->getCell(row, column)->setTable(newTable.get());
			}
		}
		return newTable;
	}

	HitoriCell* HitoriBoard::getCell(int row, int column)
	{
		HitoriCell* cell = nullptr;
		int size = this->size();
		if (row >= 0 && row < size && column >= 0 && column < size)
		{
			return &table[row][column];
		}
		return nullptr;
	}

	HitoriCell * HitoriBoard::getFirstNotIncorrectCell()
	{
		int size = this->size();
		for (int row = 0; row < size; row++)
		{
			for (int column = 0; column < size; column++)
			{
				HitoriCell* cell = getCell(row, column);
				if (!cell->isIncorrect())
				{
					return cell;
				}
			}
		}
	}

	HitoriCell * HitoriBoard::getNextNotIncorrectCell(HitoriCell* cell)
	{
		int row = cell->getRow();
		int column = cell->getColumn();
		HitoriCell* nextCell = getCell(row - 1, column);
		if (nextCell)
		{
			if (!nextCell->isIncorrect())
				return nextCell;
		}
		nextCell = getCell(row, column - 1);
		if (nextCell)
		{
			if (!nextCell->isIncorrect())
				return nextCell;
		}
		nextCell = getCell(row, column + 1);
		if (nextCell)
		{
			if (!nextCell->isIncorrect())
				return nextCell;
		}
		nextCell = getCell(row + 1, column);
		if (nextCell)
		{
			if (!nextCell->isIncorrect())
				return nextCell;
		}
	}

	HitoriCell * HitoriBoard::getNextNotIncorrectAndUntravelledCell(HitoriCell * cell, TravelTable & travelTable)
	{
		int row = cell->getRow();
		int column = cell->getColumn();
		HitoriCell* nextCell = getCell(row - 1, column);
		if (nextCell)
		{
			if (!nextCell->isIncorrect() && !travelTable[row - 1][column])
				return nextCell;
		}
		nextCell = getCell(row, column - 1);
		if (nextCell)
		{
			if (!nextCell->isIncorrect() && !travelTable[row][column - 1])
				return nextCell;
		}
		nextCell = getCell(row, column + 1);
		if (nextCell)
		{
			if (!nextCell->isIncorrect() && !travelTable[row][column + 1])
				return nextCell;
		}
		nextCell = getCell(row + 1, column);
		if (nextCell)
		{
			if (!nextCell->isIncorrect() && !travelTable[row + 1][column])
				return nextCell;
		}
		return nullptr;
	}

	HitoriCell* HitoriBoard::getFirstCellWithoutState()
	{
		for (auto column : table)
		{
			for (HitoriCell cell : column)
			{
				if (!cell.isCorrect() && !cell.isIncorrect())
					return getCell(cell.getRow(), cell.getColumn());
			}
		}
		return nullptr;
	}

	std::vector<HitoriCell*> HitoriBoard::getRow(int row)
	{
		std::vector<HitoriCell*> v;
		for (int i = 0; i < size(); i++)
		{
			v.push_back(&table[row][i]);
		}
		return v;
	}

	std::vector<HitoriCell*> HitoriBoard::getColumn(int column)
	{
		std::vector<HitoriCell*> v;
		for (int i = 0; i < size(); i++)
		{
			v.push_back(&table[i][column]);
		}
		return v;
	}

	int HitoriBoard::size() const
	{
		return table[0].size();
	}

	HitoriBoard::HitoriBoard(std::initializer_list< std::initializer_list<HitoriCell> > table)
	{
		int size = table.size();
		int rowNo = 0;
		for (auto row : table)
		{
			if (row.size() != size)
			{
				throw std::exception(
					("Size of column is not equal to size of row " +
						std::to_string(size) + "!=" + std::to_string(row.size())).c_str()
				);
			}
			this->table.push_back(row);
			for (int columnNo = 0; columnNo < this->table.back().size(); columnNo++)
			{
				HitoriCell* cell = getCell(rowNo, columnNo);
				cell->setRowColumn(rowNo, columnNo);
				cell->setTable(this);

			}
			rowNo++;
		}
	}

	std::vector<std::vector<HitoriCell>>* HitoriBoard::getTable()
	{
		return &table;
	}

	std::string HitoriBoard::toString()
	{
		std::string s = "";
		for (auto column : table)
		{
			for (HitoriCell cell : column)
			{
				s += cell.toString() + " ";
			}
			s += "\n";
		}
		return s;
	}

	int HitoriBoard::notIncorrectCells()
	{
		int notIncorrectCells = 0;
		int size = this->size();
		for (int row = 0; row < size; row++)
		{
			for (int column = 0; column < size; column++)
			{
				if (!getCell(row, column)->isIncorrect())
				{
					notIncorrectCells++;
				}
			}
		}
		return notIncorrectCells;
	}

	void HitoriBoard::unshadeAround(HitoriCell * cell)
	{
		int column = cell->getColumn();
		int row = cell->getRow();
		HitoriCell* cellToUnShade = getCell(row - 1, column);
		if (cellToUnShade)
		{
			cellToUnShade->unshade();
		}
		cellToUnShade = getCell(row + 1, column);
		if (cellToUnShade)
		{
			cellToUnShade->unshade();
		}
		cellToUnShade = getCell(row, column - 1);
		if (cellToUnShade)
		{
			cellToUnShade->unshade();
		}
		cellToUnShade = getCell(row, column + 1);
		if (cellToUnShade)
		{
			cellToUnShade->unshade();
		}

	}

	std::vector<HitoriCell>& HitoriBoard::operator[](int x)
	{
		return table[x];
	}

	std::ostream & operator<<(std::ostream & os, HitoriBoard & table)
	{
		os << whiteBackground << table.printTopBorder() << white << std::endl;
		for (int i = 0; i < table.size(); i++)
		{
			os << whiteBackground << (char)179;// ?
			for (HitoriCell cell : table[i])
			{
				os << std::setw(numDigits(table.size())) << cell << whiteBackground << (char)179;// ?
			}
			os << white << std::endl;
			os << whiteBackground << table.printBorder(i) << white << std::endl;
		}
		os << white;
		return os;
	}

	HitoriBoard::~HitoriBoard()
	{
	}
}